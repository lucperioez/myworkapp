import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,ToastController} from 'ionic-angular';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Employee } from '../../providers';
/**
 * Generated class for the MyReferencesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-references',
  templateUrl: 'my-references.html',
})
export class MyReferencesPage {
  private authForm : FormGroup;
  public user_id:any;
  public user_references:any;
  constructor( 
    public storage: Storage,
    public employee: Employee,
    public events: Events,
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    ) {
    this.user_references = [];
    events.publish('update:menu','My references');
    this.authForm = this.formBuilder.group({
      company:['', Validators.required],
      full_name: ['', Validators.required],
      phone: ['', Validators.required],
      description:['']
    })
    this.storage.get('usr').then((val) => {
      this.imprimir(val); 
    })
  }

  imprimir(val){
    this.user_id = val.user.id_user;

    this.getReferences();

  }

  getReferences(){
    this.employee.getReferences(this.user_id).subscribe((resp) => {
      this.user_references = resp['refrences'];
    }, (err) => {
    });
  }

  addReference(){
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<p><img src="assets/img/loader.gif" width="50px" padding-horizontal /><br>
                  Registrando datos espere un momento porfavor ...</p>`,
      duration: 5000
    });
    loading.present();
    loading.onDidDismiss(() => {
      let toast = this.toastCtrl.create({
        message: 'Reference created success',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      this.authForm.controls['company'].setValue("");
      this.authForm.controls['full_name'].setValue("");
      this.authForm.controls['phone'].setValue("");
      this.authForm.controls['description'].setValue("");
      this.getReferences()
    });

    this.employee.addReference(this.authForm.value,this.user_id).subscribe((resp) => {
     loading.dismiss();

    }, (err) => {
      let toast = this.toastCtrl.create({
        message: 'Reference created error',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    });
  }

  delete(id){
    this.employee.deleteReference(id).subscribe((resp) => {
      let toast = this.toastCtrl.create({
        message: 'Reference deleted success',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      this.authForm.controls['company'].setValue("");
      this.authForm.controls['full_name'].setValue("");
      this.authForm.controls['phone'].setValue("");
      this.authForm.controls['description'].setValue("");
      this.getReferences()
    }, (err) => {
      let toast = this.toastCtrl.create({
        message: 'Reference deleted error',
        duration: 2000,
        position: 'top'
      });
      toast.present();
    });
  }

  

}
