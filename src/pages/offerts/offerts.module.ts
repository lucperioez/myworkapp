import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OffertsPage } from './offerts';

@NgModule({
  declarations: [
    OffertsPage,
  ],
  imports: [
    IonicPageModule.forChild(OffertsPage),
  ],
})
export class OffertsPageModule {}
