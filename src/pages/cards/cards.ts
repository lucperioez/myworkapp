import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Employee } from '../../providers';
import { Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-cards',
  templateUrl: 'cards.html'
})
export class CardsPage {
  cardItems: any[];
  public Title:any;
  public Employees:any;
  constructor(
    public navCtrl: NavController,
    public employee: Employee,
    public events: Events
    ) {
      events.publish('update:menu','Search results');
  }

  ionViewDidLoad() {
    this.employee.getEmployees().subscribe((resp) => {
      console.log(resp);
      this.Employees = resp['users'];
    });
  }

  detail(id) {
    this.navCtrl.push('EmployeeDetailPage',{  id: id });
  }

  references(){
    this.navCtrl.push('ReferencesPage');
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.Title = page.title;
    this.navCtrl.setRoot(page.component);
  }
}
