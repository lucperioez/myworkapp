import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Events } from 'ionic-angular';
/**
 * Generated class for the SearchOffersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-offers',
  templateUrl: 'search-offers.html',
})
export class SearchOffersPage {

  params: Object;
  pushPage: any;
  public user_cities:any;
  public work_type:any;
  public start_date:any;
  public end_date:any;
  public start_hour:any;
  public end_hour:any;
  constructor(public events: Events,public navCtrl: NavController) { 
    events.publish('update:menu','Find Offers');
  }

  clear() {
    this.user_cities="";
    this.work_type="";
    this.start_date="";
    this.end_date="";
    this.start_hour="";
    this.end_hour="";
  }


  search(){
    let search_object = {
      "cities":this.user_cities,
      "work_type":this.work_type,
      "start_date":this.start_date,
      "end_date":this.end_date,
      "start_hour":this.start_hour,
      "end_hour":this.end_hour
    }
    console.log(search_object);
  }

}
