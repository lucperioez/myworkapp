import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';
import { FirstRunPage } from '../pages';
import { Settings } from '../providers';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
import { Advertising } from '../providers';
import { Observable } from 'rxjs/Rx';
import { Slides } from 'ionic-angular';
@Component({
  template: `
<ion-menu [content]="content" *ngIf="logged" class="main-content">
    <ion-header>
      <ion-toolbar>
        <ion-title>Pages</ion-title>
      </ion-toolbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">
        <ion-icon name="{{p.icon}}">&nbsp;&nbsp;&nbsp;</ion-icon>{{p.title}}
        </button>
        <button menuClose ion-item (click)="logout()" color="danger">
        <span style="color:white!important;"><ion-icon   name="close-circle">&nbsp;&nbsp;&nbsp;</ion-icon>Logout</span>
        </button>
       
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-header *ngIf="logged">

  <ion-navbar>
    <ion-buttons start>

    </ion-buttons>
    <button ion-button menuToggle>
      <ion-icon name="menu" class="menu-icon"></ion-icon>
    </button>
    <ion-title>
      {{Title}}
    </ion-title>
    <ion-buttons end>
      <button (click)="openPage({ title: 'Config', component: 'ConfigPage' })" ion-button>
        <ion-icon name="contact" class="porfile-icon" ></ion-icon>
      </button>
    </ion-buttons>
  </ion-navbar>

</ion-header>

  <ion-nav #content  [root]="rootPage"></ion-nav>
  
  <ion-footer *ngIf="logged">
  <ion-slides *ngIf="PImage && PImage.length" (ionSlideDidChange)="slideChanged()" autoplay="5000" loop="true" speed="2000" class="slides" pager="true">
  <ion-slide *ngFor="let Image of PImage">
     <img src="http://54.183.200.188/workapp-backend{{Image.url}}" alt="Product Image">
    </ion-slide>
  </ion-slides>
  </ion-footer>
  `
})
export class MyApp {
  @ViewChild(Slides) slides: Slides;
  rootPage = FirstRunPage;
  public Title: String;
  public logged:boolean;
  subscription:any;
  @ViewChild(Nav) nav: Nav;
  pages : any[];
  pages_buldier: any[] = [
    { title: 'FIND EMPLOYEES', component: 'SearchPage', icon: 'search' },
    { title: 'MY OFFERTS', component: 'OffertsPage', icon: 'clipboard' },
    { title: 'MY TEAM', component: 'MyTeamPage', icon: 'contacts' },
    { title: 'CONFIG', component: 'ConfigPage' , icon: 'settings'}
  ]

  pages_employee: any[] = [
    { title: 'FIND OFFERS', component: 'SearchOffersPage', icon: 'search' },
    { title: 'MY REFERENCES', component: 'MyReferencesPage', icon: 'checkmark-circle' },
    { title: 'CONFIG', component: 'ConfigPage', icon: 'settings' }
  ]
  PImage: any;
  constructor(public advertising: Advertising,public events: Events,private translate: TranslateService, platform: Platform, settings: Settings, private config: Config, private statusBar: StatusBar, private splashScreen: SplashScreen,public storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.initTranslate();
    this.storage.get('usr').then((val) => {
      if(val != null && val != undefined){
        if(val){
          this.logged == true;
          switch (val.user.role_id) {
            case '2':
              this.pages = this.pages_buldier;
              break;
            case '3':
              this.pages = this.pages_employee;
              break;
            default:
              break;
          }
        }
      }
     

      
    })
    events.subscribe('update:menu', (Title) => {
      console.log(Title);
      this.Title = Title;
      this.logged = true;
      this.storage.get('usr').then((val) => {
        if(val != null && val != undefined){
          if(val){
            
            switch (val.user.role_id) {
              case '2':
                this.pages = this.pages_buldier;
                break;
              case '3':
                this.pages = this.pages_employee;
                break;
              default:
                break;
            }
          }
        }
      })
    });

    events.subscribe('close:menu', () => {
      this.logged = false;
      console.log('entro');
    });
    
    this.advertising.get().subscribe((resp) => {
      console.log(resp);
      this.PImage = resp;
    }, (err) => {

    });

    let i = 0;
    console.log('hola');
    this.subscription = Observable.interval(10000).subscribe(x => {
    // the number 1000 is on miliseconds so every second is going to have an iteration of what is inside this code.
     
      i++;
    })
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', currentIndex);
      if(currentIndex == this.PImage.length){
        this.advertising.get().subscribe((resp) => {
          console.log(resp);
          this.PImage = resp;
        }, (err) => {
  
        });
      }

  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  openPage(page) {
 
    this.nav.setRoot(page.component);
  }


  logout(){
    let TIME_IN_MS = 500;
    this.storage.remove('usr');
    let hideFooterTimeout = setTimeout( () => {
      this.nav.setRoot('LoginPage');
    }, TIME_IN_MS);
    
    
  }
}
